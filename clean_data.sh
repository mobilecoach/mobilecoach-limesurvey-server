#!/bin/bash

read -p "Do you want to clean everything, including the database (y/n)? " CONT
if [ "$CONT" != "y"  -a "$CONT" != "Y" ]; then
  exit;
fi

cd mysql_data
rm -r -f *
cd ..

