#!/bin/bash

echo "Running this script will remove the changes made in the contents of the folder."
git clean -fd
git reset --hard

echo "Please enter the hostname of the server (example: workshop-cdhi.ethz.ch):"
read -e domain

echo "Please enter the limesurvey port (example: 8080):"
read -e limesurvey_port

echo "Please enter the path to letencrypt (example: ../mobilecoach-server/letsencrypt/)"
read -e letencrypt_path

echo "The informations provided are hostname: $domain, limesurvey port: $limesurvey_port and letencrypt path: $letencrypt_path."
echo "Are you sure of these details, and used this script with 'sudo' and want to continue setup? If okay, please answer with 'yes':"
read -e ANSWER

if [ ! "$ANSWER" = "yes" ]
  then
  echo "Please restart the script to enter the information again."
  exit 0
fi

ln -s $letencrypt_path .

mysqlrootpass=`openssl rand -hex 8`
mysqluserpass=`openssl rand -hex 8`

find . -type f -exec sed -i 's/<hostname>/'"$domain"'/g' {} +
find . -type f -exec sed -i 's/<port_limesurvey>/'"$limesurvey_port"'/g' {} +
find . -type f -exec sed -i 's/<mysql_root_password>/'"$mysqlrootpass"'/g' {} +
find . -type f -exec sed -i 's/<mysql_user_password>/'"$mysqluserpass"'/g' {} +

